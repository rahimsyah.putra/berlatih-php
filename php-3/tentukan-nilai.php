<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    function tentukan_nilai($number)
    {
        //  kode disini
        if ($number >= 85 && $number <= 100){
            echo "Sangat Baik";
            echo "<br>";
        }else if($number >= 70 && $number < 85){
            echo "Baik";
            echo "<br>";
        }else if($number >= 60 && $number < 70){
            echo "Cukup";
            echo "<br>";
        }else{
            echo "Kurang";
        }
    }

    //TEST CASES
    echo tentukan_nilai(98); //Sangat Baik
    echo tentukan_nilai(76); //Baik
    echo tentukan_nilai(67); //Cukup
    echo tentukan_nilai(43); //Kurang
    ?>
</body>

</html>