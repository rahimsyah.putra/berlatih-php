<?php

    require('animal.php');
    require('frog.php');
    require('ape.php');

    $sheep = new Animal("shaun");
    echo "Nama: $sheep->name <br>"; // "shaun"
    echo "Jumlah kaki: $sheep->legs <br>"; // 2
    echo "Berdarah dingin: $sheep->cold_blooded <br>";// false

    echo "<br>";

    $sungokong = new Ape("kera sakti");
    echo "Nama: $sungokong->name <br>";
    echo "Jumlah kaki: $sungokong->legs <br>";
    echo "Berdarah dingin: $sungokong->cold_blooded <br>";
    $sungokong->yell()  ;// "Auooo"

    echo "<br>";

    $kodok = new Frog("buduk");
    echo "Nama: $kodok->name <br>";
    echo "Jumlah kaki: $kodok->legs <br>";
    echo "Berdarah dingin: $kodok->cold_blooded <br>";
    $kodok->jump() ; // "hop hop"

?>