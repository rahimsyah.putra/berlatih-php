<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required'
        ]);

        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);

        return redirect('/pertanyaan')->with('success', 'Post Berhasil Dibuat');
    }

    public function index(){
        $posts = DB::table('pertanyaan')->get();
        // dd($posts);
        return view('posts.index', compact('posts'));
    }

    public function show($id){
        $show = DB::table('pertanyaan')->where('id', $id)->first(); 
        // dd($show);
        return view('posts.show', compact('show'));  
    }

    public function edit($id)
    {
        $post = DB::table('pertanyaan')->where('id', $id)->first(); 

        return view('posts.edit', compact('post'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required'
        ]);

        $query = DB::table('pertanyaan')
                    ->where('id', $id)
                    ->update([
                        'judul' => $request['judul'],
                        'isi' => $request['isi']
                    ]);
        return redirect('/pertanyaan')->with('success', 'Post berhasil diperbaharui');
    }

    public function destroy($id)
    {
        $query = DB::table('pertanyaan')
                    ->where('id', $id)
                    ->delete();
        return redirect('/pertanyaan')->with('success', 'Post berhasil dihapus');
    }
}
