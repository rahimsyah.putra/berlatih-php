<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
    <form action="/welcome" method="POST">
        @csrf
        <h1>Buat Account Baru!</h1>
        <h2>Sign Up Form</h2>

        <div>
            <p>First Name:</p>
            <input type="text" id="fname" name="fname">
            <p>Last Name:</p>
            <input type="text" id="lname" name="lname">
        </div>

        <div>
            <p>Gender:</p>
            <input type="radio" name="gender" value="male">
            <label for="male">Male</label><br>
            <input type="radio" name="gender" value="female">
            <label for="female">Female</label><br>
            <input type="radio" name="gender" value="other">
            <label for="other">Other</label><br>
        </div>

        <div>
            <p>Nationality:</p>
            <select>
                <option>Indonesian</option>
                <option>Indian</option>
                <option>Australian</option>
            </select>
        </div>

        <div>
            <p>Language Spoken:</p>
            <input type="checkbox" name="language" value="indonesia">
            <label for="indonesia">Bahasa Indonesia</label><br>
            <input type="checkbox" name="language" value="english">
            <label for="english">English</label><br>
            <input type="checkbox" name="language" value="other">
            <label for="other">Other</label><br>

            <p>Bio:</p>
            <textarea cols="30" rows="10"></textarea><br>
        </div>

        <div>
            <input 
            type="submit" 
            value="Sign Up"
            name="input">
        </div>
    </form>
</body>
</html>